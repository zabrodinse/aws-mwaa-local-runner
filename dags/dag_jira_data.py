import sys, pytz, requests, json, yaml, pandas as pd, logging, boto3
from datetime import datetime
from airflow import DAG
from airflow.operators.python import PythonOperator, BranchPythonOperator
from datetime import datetime, date
from requests.auth import HTTPBasicAuth
from confluent_kafka import Producer, KafkaException
from math import ceil
from airflow.models import Variable, DagRun

TypeWork = 'Increment'
#TypeWork = 'Full update'

# Common parameters
ConfigPath = 's3://dwh-app/input/atlassian/jira/'
TargetTopic = "atlassian.api.jira.test"
headers = {'content-type': 'application/cloudevents+json; charset=UTF-8'}
DagName = "jira_load_from_api_data"
auth = HTTPBasicAuth('svc_jira_api@tinkoff.ph', 'GQuajswS6ke4SFTnypmr2B26')
KafkaVarID = "dwh-kafka-etl-producer-dev"
logger = logging.getLogger(__name__)
IssueSchemaPath = 's3://dwh-app/input/atlassian/jira/issue_schema.json'

utc=pytz.timezone('Europe/Moscow')
#today = utc.localize(datetime.combine(date.today(),datetime.min.time()))
StartDTTM = datetime.now(utc)
StartDTTMString = str(StartDTTM.replace(microsecond=0).isoformat('T'))

def SendToKafka(ListMsg, TaskID):
    # Initialize producer
    KafkaConfiguration = Variable.get(KafkaVarID, deserialize_json=True)
    KafkaConfiguration['client.id'] = "dwh.etl.{}.{}".format(DagName, TaskID)
    KafkaConfiguration['transactional.id'] = "dwh.etl.{}.{}".format(DagName, TaskID)
    producer = Producer(KafkaConfiguration)
    producer.init_transactions()
    producer.begin_transaction()
    
    for message in ListMsg:
        msg = json.dumps(message, ensure_ascii=False).encode('utf8')
        try:
            producer.produce(topic = TargetTopic, value = msg.decode(), key = message['data']['JiraTask'], headers = headers)
        except cimpl.KafkaException as err:
            logger.error('Something wrong while sending to kafka message {0}\nError: {1}'.format(message['data'], err))
            raise
        
    # Ending producer
    producer.flush(10.0)
    while True:
       try:
           producer.commit_transaction(10.0)
           break
       except KafkaException as e:
           if e.args[0].retriable():
              # retriable error, try again
              continue
           elif e.args[0].txn_requires_abort():
              producer.abort_transaction()
           else:
               # treat all other errors as fatal
               raise

def GetLastSuccessRun(dag_id): #Should use start date instead. Will fised in DWH-68
    dag_runs = DagRun.find(dag_id=dag_id, state = 'success')
    dag_runs.sort(key=lambda x: x.execution_date, reverse=True)
    return dag_runs[0].execution_date if dag_runs else None

def CheckUpdate(ti):
    LastSuccessDTTM = GetLastSuccessRun(DagName)
    DiffMinute = ceil((StartDTTM - LastSuccessDTTM).total_seconds() / 60) # round to highest
    if TypeWork == 'Increment':
        inc = 'updated%20%3E%20-{0}m'.format(DiffMinute)
    else:
        inc = ''
    UrlAllBasic = "https://serenitycloud.atlassian.net/rest/api/2/search?jql={0}&fields=created&maxResults=100&startAt=".format(inc)
    
    IssueList = []
    NewIssueList = []
    i = 0
    TotalRow = 1 # Just any number for triggering the loop condition, we will redefine it further
    
    while TotalRow > 100*i: # The loop condition can be replaced with "isLast == False", where isLast = jsonResponse ['isLast']
        UrlAll = UrlAllBasic + str(100*i)
        response = requests.get(UrlAll, auth = auth)
        jsonResponse = response.json()
        TotalRow = jsonResponse['total'] # If an empty query is returned, then TotalRow will be 0
        IssuesListResponse = jsonResponse['issues']
        
        for idx, val in enumerate(IssuesListResponse):
            CreatedStr = val['fields']['created']
            CreatedDttm = datetime.strptime(CreatedStr, '%Y-%m-%dT%H:%M:%S.%f%z')
            IssueList.append(val['key'])
            if CreatedDttm > LastSuccessDTTM or TypeWork == 'Full update': # New task will be needed after.
                NewIssueList.append(val['key'])
        i = i + 1
    
    if IssueList != []:
        ti.xcom_push(key='JiraNewIssue', value = NewIssueList)
        ti.xcom_push(key='JiraUpdateIssue', value = IssueList)
        return "GetUpdate"
    else:
        return "NoUpdate"

def LoadSchema(s3_path):
    logger.info("Load all schemas and their fields")
    path_parts = s3_path.replace("s3://", "").split("/")
    bucket = path_parts.pop(0)
    key = "/".join(path_parts)
    s3 = boto3.client('s3')
    file_schema = s3.get_object(Bucket=bucket, Key=key)
    schema = json.load(file_schema["Body"])
    return schema

def GetUpdate(ti, **context):
    message_pre = {
    'specversion' : '1.0',
    'type' : 'net.atlassian.serenitycloud.issue.v1',
    'datacontenttype' : 'application/json',
    'source' : 'https://serenitycloud.atlassian.net/rest/api/2/issue/TaskKey/changelog',
    'dataschema' : ConfigPath + 'changelog_schema.json',
    'datasecurity' : ConfigPath + 'changelog_security.yml',
    'subject' : 'changelog'
    }
    ListChanges = []
    ListChangesComponent = []
    ListMessage = []
    IssueList = ti.xcom_pull(key='JiraUpdateIssue', task_ids='CheckUpdate')
    NewIssueList = ti.xcom_pull(key='JiraNewIssue', task_ids='CheckUpdate')
    LastSuccessDTTM = GetLastSuccessRun(DagName)
    for task in IssueList:
        UrlUpdateBasic = 'https://serenitycloud.atlassian.net/rest/api/2/issue/{0}/changelog/?maxResults=100&startAt='.format(task)
        i = 0
        TotalRow = 1
    
        while TotalRow > 100*i:
            UrlUpdate = UrlUpdateBasic + str(100*i)
            response = requests.get(UrlUpdate, auth = auth)
            jsonResponse = response.json()
            TotalRow = jsonResponse['total']
            ChangesListResponse = jsonResponse['values']
            
            for val in ChangesListResponse:
                if datetime.strptime(val['created'], '%Y-%m-%dT%H:%M:%S.%f%z') > LastSuccessDTTM or TypeWork == 'Full update':
                    valAdj = {
                    'JiraTask' : task,
                    'ChangeId' : val['id'],
                    'AutorId' : val['author']['accountId'],
                    'CreateDTTM' : datetime.strptime(val['created'], '%Y-%m-%dT%H:%M:%S.%f%z').replace(microsecond=0).isoformat('T'),
                    'ChangeList' : val['items']
                    } # only the fields we need from dictionary
                    
                    message_pre['id'] = valAdj['ChangeId']
                    message_pre['time'] = valAdj['CreateDTTM']
                    message_pre['data'] = valAdj
                    ListMessage.append(message_pre.copy())
                    
                    #Write all previous values for initial state of task
                    if task in NewIssueList:
                        for changes in val['items']:
                            try:
                                fieldId = changes['fieldId']
                                if fieldId == 'components': #Field "components' has unique type of updating.
                                    ListChangesComponent.append([task, changes['from'], changes['to'], val['created']])
                            except (TypeError, KeyError):
                                fieldId = changes['field']
                            if changes['from'] is not None:
                                previous = changes['from']
                            else:
                                previous = changes['fromString']
                            ListChanges.append([task, fieldId, previous, val['created']])
            i = i + 1
    SendToKafka(ListMessage, context['task'].task_id)
    logger.info('Updates send succesfully, number of messages: {}'.format(len(ListMessage)))
    
    #get initial state of task
    ChangesDF = pd.DataFrame(ListChanges, columns=['JiraTask', 'Field', 'Previous', 'ChangesDTTM']).sort_values(['JiraTask', 'ChangesDTTM'], ascending=True)
    ChangesGrouped = ChangesDF.groupby(['JiraTask', 'Field'])
    ChangesFirst = ChangesGrouped.first().reset_index().drop('ChangesDTTM', 1)
    FirstValuesDict = ChangesFirst.set_index(['JiraTask', 'Field']).T.to_dict('index')
    # result, e.g. {'Previous': {('WF-3', 'assignee'): None, ('WF-3', 'description'): None, ('WF-3', 'status'): '10155'}}
    ComponentDF = pd.DataFrame(ListChangesComponent, columns=['JiraTask', 'Previous', 'Next', 'ChangesDTTM']).sort_values(['JiraTask', 'ChangesDTTM'], ascending=False)

    # New Task - full list fields
    message_pre = {
    'specversion' : '1.0',
    'type' : 'net.atlassian.serenitycloud.issue.v1',
    'datacontenttype' : 'application/json',
    'source' : 'https://serenitycloud.atlassian.net/rest/api/2/TaskKey',
    'dataschema' : ConfigPath + 'issue_schema.json',
    'datasecurity' : ConfigPath + 'issue_security.yml',
    'subject' : 'issue'
    }
    ListMessage = []
    
    SchemaDict = {}
    if NewIssueList != []:
        IssueSchema = LoadSchema(IssueSchemaPath)
        for key in IssueSchema['properties']: # Order key will be diffrenet for tasks, so we get it from schema
            SchemaDict[key] = None
    for task in NewIssueList:
        UrlIssue = 'https://serenitycloud.atlassian.net/rest/api/2/issue/{0}'.format(task)
        response = requests.get(UrlIssue, auth = auth)
        jsonResponse = response.json()
        FieldDictionary = jsonResponse['fields']
        
        FieldAdj = SchemaDict.copy()
        FieldAdj['JiraTask'] = task
        FieldAdj['JiraTaskId'] =  jsonResponse['id']
        FieldAdj['DownloadDTTM'] =  StartDTTMString
        
        # Fields don't have any example - so we cann't unclude them to one of lists below. We just copy them.
        ListWithoutExamplesFiled = ['customfield_10022', 'customfield_10023', 'customfield_10010', 'customfield_10005', 'customfield_10006', 'security', 
        'customfield_10007', 'customfield_10008', 'customfield_10009', 'customfield_10040', 'customfield_10000', 'customfield_10001', 'customfield_10002',
        'customfield_10003', 'customfield_10004', 'environment', 'customfield_10041', 'customfield_10042']
        
        # Filed have id and value, but value can change on its own, so we load with task only id, name will be loaded as dictionary.
        ListWithIdField = ['customfield_10035', 'issuetype', 'project', 'customfield_10005', 'customfield_10006', 'customfield_10007', 'customfield_10040', 'customfield_10004',
        'priority', 'status', 'resolution', 'customfield_10012', 'customfield_10036', 'parent', 'customfield_10020', 'customfield_10045', 'customfield_10051']

        # The same as ListWithIdField, but instead id we have accountId
        ListWithAccountIdField = ['reporter', 'assignee', 'creator', 'customfield_10043', 'customfield_10042' , 'customfield_10046', 'customfield_10047', 
        'customfield_10048', 'customfield_10049', 'customfield_10050']

        # The same as ListWithIdField, but field can have mane Ids
        ListWithManyIdField = ['fixVersions', 'components', 'customfield_10039', 'customfield_10021', 'subtasks', 'versions']
        
        # Special fields, not like others
        ListSpecialField = ['labels']
        
        # Jira DTTM format doesn't fit SF's format
        ListDTTMField = ['resolutiondate', 'created', 'customfield_10024']

        ListNotLoadField = ['comment', 'worklog', 'attachment', 'customfield_10018', 'watches', 'votes', 'issuerestriction', 'timetracking', 'aggregateprogress', 
        'progress', 'lastViewed', 'issuelinks', 'statuscategorychangedate', 'updated', 'customfield_10013', 'customfield_10011']
        # Comment is taken by another request
        # Worklog, attachment isn't taken - if necessary, we will load it separately
        # Customfield_10018 gives up parent tasks, not available to us.
        # Watches, votes changes don't log into changelog. If we want to analyze this, we need a another way to load data.
        # Issuerestriction gives security level set on issue - hard to understand for what this data us
        # Timetracking, aggregateprogress, progress repeat timeoriginalestimate, timeestimate and timespent
        # lastViewed, updated, statuscategorychangedate don't have any value
        # issuelinks allways Null when task creating
        # customfield_10013 and customfield_10011 exists only for epic and dublicate other fields
        
        ListNotSimpleField = ListWithIdField + ListWithAccountIdField + ListWithManyIdField + ListSpecialField + ListNotLoadField + ListDTTMField

        # Copy simply fields as is
        for key in list(FieldDictionary.keys()):
            if key not in ListNotSimpleField:
                val = FieldDictionary[key]
                if type(val) is str or type(val) is int:
                    FieldAdj[key] = val # It is simple field with only value.
                if type(val) is dict:
                    if 'id' in val and 'self' in val:
                        ListWithIdField.append(key) # Field has "self' and "id" elements, so we can load it with the fields from ListWithIdField.
                    if 'accountId' in val and 'self' in val:
                        ListWithAccountIdField.append(key) # Field has "self' and "accountId" elements, so we can load it with the fields from ListWithAccountIdField.
                    else:
                        FieldAdj[key] = val # Otherways, just load data as is.
                if type(val) is list and val != []:
                    if type(val[0]) is dict and 'id' in val[0] and 'self' in val[0]:
                        ListWithManyIdField.append(key) # Field has "self' and "id" elements in list, so we can load it with other fields from ListWithManyIdField.
                    else:
                        FieldAdj[key] = val # Otherways, just load data as is.
                else:
                    FieldAdj[key] = FieldDictionary[key] # Otherways, just load data as is.

        # Adding complex fields
        for val in ListWithIdField:
            try:
                FieldAdj[val] = FieldDictionary[val]['id']
            except (TypeError, KeyError):
                FieldAdj[val] = None
                
        for val in ListWithAccountIdField:
            try:
                FieldAdj[val] = FieldDictionary[val]['accountId']
            except (TypeError, KeyError):
                FieldAdj[val] = None       
        
        for val in ListWithManyIdField:
            try:
                ListId = []
                for newval in FieldDictionary[val]:
                    ListId.append(newval['id'])
                FieldAdj[val] = ', '.join(ListId)
            except (TypeError, KeyError):
                FieldAdj[val] = None      

        try:
            LabelList = FieldDictionary['labels']
            FieldAdj['labels'] = ', '.join(LabelList)
        except (TypeError, KeyError):
            FieldAdj['labels'] = None
                
        for val in ListDTTMField:
            try:
                FieldAdj[val] = datetime.strptime(FieldDictionary[val], '%Y-%m-%dT%H:%M:%S.%f%z').replace(microsecond=0).isoformat('T')
            except (ValueError,TypeError):
                FieldAdj[val] = None
        
        #Fields below is costyl! Переделаем когда автоматом будем добавлять в схему и таблицу поля. Will fised in DWH-68
        try:
            FieldAdj['customfield_10052'] = FieldDictionary['customfield_10052']
        except (TypeError, KeyError):
            FieldAdj['customfield_10052'] = None
            
        try:
            FieldAdj['customfield_10053'] = FieldDictionary['customfield_10053']
        except (TypeError, KeyError):
            FieldAdj['customfield_10053'] = None

        try:
            FieldAdj['customfield_10044'] = FieldDictionary['customfield_10044']
        except (TypeError, KeyError):
            FieldAdj['customfield_10044'] = None
        ''' Linked task will get only from updates.
        try:
            LinkTask = []
            for val in FieldDictionary['issuelinks']:
                LinkTaskAttribute = {}
                LinkTaskAttribute['LinkId'] = val['id']
                LinkTaskAttribute['LinkTypeId'] = val['type']['id']
                LinkTaskAttribute['LinkTypeName'] = val['type']['name']
                LinkTaskAttribute['LinkTypeInward'] = val['type']['inward']
                LinkTaskAttribute['LinkTypeOutward'] = val['type']['outward']
                try:
                    LinkTaskAttribute['LinkOutwardIssueId'] = val['outwardIssue']['id']
                except (TypeError, KeyError):
                    LinkTaskAttribute['LinkOutwardIssueId'] = None
                try:
                    LinkTaskAttribute['LinkInwardIssueId'] = val['inwardIssue']['id']
                except (TypeError, KeyError):
                    LinkTaskAttribute['LinkInwardIssueId'] = None            
                LinkTask.append(LinkTaskAttribute)
            FieldAdj['IssueLink'] = LinkTask
        except (TypeError, KeyError):
            FieldAdj['IssueLink'] = None
        '''
            
        # Change value of field to initial state
        ListNullField = ['resolutiondate', 'workratio', 'parent', 'subtasks'] # Always Null when task is created and have some problems with update (different names, no updates, etc)
        ListNumberFiled = ['customfield_10053', 'customfield_10052', 'customfield_10020', 'aggregatetimeestimate', 'aggregatetimeoriginalestimate', 'timeoriginalestimate', 'customfield_10016', 'workratio', 'aggregatetimespent', 'timespent', 'timeestimate']
        # Another Kostyl - redone in DWH-68
        for val in FieldAdj:
            if val != 'components':
                if (task, val) in FirstValuesDict['Previous']:
                    if val in ListNumberFiled:
                        FieldAdj[val] = float(FirstValuesDict['Previous'][(task, val)])
                    else:
                        FieldAdj[val] = FirstValuesDict['Previous'][(task, val)]
                if (task, 'Key') in FirstValuesDict['Previous']: # Some problem with name field "key"
                    FieldAdj['JiraTask'] = FirstValuesDict['Previous'][(task, 'Key')]
            elif task in ComponentDF['JiraTask'].unique().tolist():
                TaskDF = ComponentDF[ComponentDF['JiraTask'] == task]
                ListComponent = FieldAdj[val].split(', ')
                for index, row in TaskDF.iterrows():
                    if row['Previous'] is None:
                        ListComponent.remove(row['Next'])
                    if row['Next'] is None:
                        ListComponent.append(row['Previous'])
                if ListComponent != []:
                    FieldAdj[val] = ', '.join(ListComponent)
                else:
                    FieldAdj[val] = None
            '''С компонентой полная хрень - обновление происходит для кадждого ID-ка по отдельности (пример PHB-49). 
            Поэтому мы в отдельном DF проходим с самого последнего до первого изменения и удаляем или добавляем компоненты'''
        for val in ListNullField:
            FieldAdj[val] = None
    
        # Drop keys wich not represented in schema. Will fised in DWH-68
        KeyForDel = []
        for key in FieldAdj:
            if key not in SchemaDict.keys():
                KeyForDel.append(key)
        for key in KeyForDel:
            del FieldAdj[key]

        message_pre['id'] = FieldAdj['JiraTaskId'] + ' ' + StartDTTMString
        message_pre['time'] = StartDTTMString
        message_pre['data'] = FieldAdj
        ListMessage.append(message_pre.copy())
    SendToKafka(ListMessage, context['task'].task_id)
    logger.info('Issues send succesfully, number of messages: {}'.format(len(ListMessage)))
    
    ''' For change schema file in s3. Should do the same with gitlab.
    (bucket, key) = split_s3_path(ConfigPath)
    s3 = boto3.client('s3')
    file_schema = s3.get_object(Bucket=bucket, Key=key+'issue_schema.json')
    data_schema = json.load(file_schema["Body"])
    file_security = s3.get_object(Bucket=bucket, Key=key +'issue_security.yml')
    data_security = yaml.load(file_security["Body"])
    NewFieldList = []
         
        # Get new fields. This part was in loop above.
        for val in FieldAdj: 
            if val not in data_schema['properties'] and val not in NewFieldList:
                NewFieldList.append(val)
    
    # Add field to config
    if NewFieldList != []: # Run only if we have a new field.
        # Backup old config 
        body_schema = json.dumps(data_schema, ensure_ascii=False, indent = 4)
        body_security = yaml.dump(data_security, sort_keys=False)
        s3.put_object(Body = body_schema, Bucket=bucket, Key=key+'field_schema_backup_{}.json'.format(date.today()))
        s3.put_object(Body = body_security, Bucket=bucket, Key=key+'field_security_backup_{}.yml'.format(date.today()))
        
        for val in NewFieldList: 
            data_schema['properties'][val] = {}
            data_schema['properties'][val]['type'] = '[string, null]' # Default value, otherwise the file will be updated manually
            data_security['security'][val] = 'OPN'

        body_schema_new = json.dumps(data_schema, ensure_ascii=False, indent = 4)
        body_security_new = yaml.dump(data_security, sort_keys=False)
        s3.put_object(Body = body_schema_new, Bucket=bucket, Key=key+'field_schema.json'.format(date.today()))
        s3.put_object(Body = body_security_new, Bucket=bucket, Key=key+'field_security.yml'.format(date.today()))
    '''

def GetComment(ti, **context):
    LastSuccessDTTM = GetLastSuccessRun(DagName)
    message_pre = {
    'specversion' : '1.0',
    'type' : 'net.atlassian.serenitycloud.issue.v1',
    'datacontenttype' : 'application/json',
    'source' : 'https://serenitycloud.atlassian.net/rest/api/2/issue/TaskKey/comment',
    'dataschema' : ConfigPath + 'commet_schema.json',
    'datasecurity' : ConfigPath + 'comment_security.yml',
    'subject' : 'comment'
    }
    IssueList = ti.xcom_pull(key='JiraUpdateIssue', task_ids='CheckUpdate')
    ListMessage = []
    for task in IssueList:
        UrlCommentBasic = 'https://serenitycloud.atlassian.net/rest/api/2/issue/{0}/comment/?maxResults=100&startAt='.format(task)
        i = 0
        TotalRow = 1
        
        while TotalRow > 100*i:
            UrlComment = UrlCommentBasic + str(100*i)
            response = requests.get(UrlComment, auth = auth)
            jsonResponse = response.json()
            TotalRow = jsonResponse['total']
            CommentListResponse = jsonResponse['comments']
    
            for val in CommentListResponse:
                if datetime.strptime(val['updated'], '%Y-%m-%dT%H:%M:%S.%f%z') > LastSuccessDTTM or TypeWork == 'Full update':
                    valAdj = {
                    'JiraTask' : task,
                    'CommentId' : val['id'],
                    'AutorId' : val['updateAuthor']['accountId'],
                    'UpdateDTTM' : datetime.strptime(val['updated'], '%Y-%m-%dT%H:%M:%S.%f%z').replace(microsecond=0).isoformat('T'),
                    'CommentBody' : val['body']
                    }
                    
                    message_pre['id'] = valAdj['CommentId']
                    message_pre['time'] = valAdj['UpdateDTTM']
                    message_pre['data'] = valAdj
                    ListMessage.append(message_pre.copy())
            i = i + 1
    SendToKafka(ListMessage,context['task'].task_id)
    logger.info('Comments send succesfully, number of messages: {}'.format(len(ListMessage)))

def NoUpdate():
    logger.info('There are no updates at {}'.format(StartDTTMString))

default_args = {
'email' : ['o.vyglovskiy@tinkoff.ru'],
'email_on_failure' : True
}

dag = DAG(
    DagName,
    schedule_interval="5,20,35,50 0-23 1-31 * *",
    start_date=datetime(2021, 12, 1),
    tags=['jira'],
    catchup=False,
    default_args = default_args
)
CheckUpdateTask = BranchPythonOperator (task_id = 'CheckUpdate', dag = dag, python_callable = CheckUpdate)
GetUpdateTask = PythonOperator (task_id = 'GetUpdate', dag = dag, python_callable = GetUpdate, provide_context=True)
GetCommentTask = PythonOperator (task_id = 'GetComment', dag = dag, python_callable = GetComment, provide_context=True)
NoUpdateTask = PythonOperator (task_id = 'NoUpdate', dag = dag, python_callable = NoUpdate)


CheckUpdateTask >> [GetUpdateTask, NoUpdateTask]
GetUpdateTask >> GetCommentTask

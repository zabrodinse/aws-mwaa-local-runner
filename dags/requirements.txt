--constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.0.2/constraints-3.7.txt"

fastavro == 1.4.0
avro == 1.10.1
boto3
confluent-kafka == 1.7.0
#snowflake-connector-python == 2.4.2
#requests>=2.26.0 # требуется пакетом выше, но противоречи констрэинтам сверху
apache-airflow[snowflake] >= 1.3.0
snowflake-sqlalchemy == 1.2.4


#apache-airflow-providers-snowflake==1.3.0
#snowflake-connector-python >=2.4.1
#snowflake-sqlalchemy >=1.1.0 \
